#!/bin/bash

install_apt_programs() {
    sudo apt update
    apt_programs=("sed" "curl" "i3" "i3status" "i3blocks" "python3-pip" "vim" "vlc" "synapse" "snapd" "git" "blueman" "pavucontrol" "feh" "lxappearance" "arc-theme" "tmux" "imagemagick" "zathura" "ccls" "cmake" "bash-completion" "gdb" "autokey-gtk" "default-jdk" "default-jre" "maven" "tree" "arandr" "xournal" "jq" "cargo" "xclip" "gettext" "clangd" "ninja-build" "python3-virtualenv" "shfmt")
    sudo apt --ignore-missing install -y ${apt_programs[@]}

    if [ "$MATKO_ENV" = "INEO" ]; then
        echo "INEO ENV installing ubuntu programs"
        sudo apt --ignore-missing install -y ${ubuntu_programs[@]}
    fi
}

install_snap_programs() {
    sudo snap install core

    if [ "$MATKO_ENV" = "HOME" ]; then
        echo "HOME ENV installing ubuntu programs"
        snap_programs=("core" "bitwarden", "pycharm-community --classic", "discord" "slack")
        for ((i = 0; i < ${#snap_programs[@]}; i++)); do
            sudo snap install ${snap_programs[$i]}
        done
    fi

}

install_pip_programs() {
    venv create main
    venv main
    pip_programs=("cmake-language-server" "python-lsp-server" "jedi-language-server" "rope" "pyflakes" "mccabe" "pycodestyle" "pydocstyle" "autopep8" "yapf" "flake8" "pylint")

    for ((i = 0; i < ${#pip_programs[@]}; i++)); do
        pip3 install ${pip_programs[$i]}
    done
}

install_npm_programs() {
    npm_programs=("bash-language-server" "vim-language-server")

    for ((i = 0; i < ${#snap_programs[@]}; i++)); do
        sudo npm install -g install ${snap_programs[$i]}
    done
}

install_custom_programs() {
    mkdir -p ~/.local/bin

    ##### rustup #####
    if ! command -v rustup &>/dev/null; then
        echo "rustup already exists, skipping"
    else
        sudo apt remove rustc
        curl --proto '=https' --tlsv1.3 https://sh.rustup.rs -sSf | sh
    fi

    ##### viu #####
    if [ -d ~/apps/viu ]; then
        echo "~/apps/viu already exists, skipping"
    else
        git clone https://github.com/atanunq/viu.git ~/apps/viu

        cd ~/apps/viu/
        cargo install --path .

        rm -f ~/.local/bin/viu
        ln -s ~/apps/viu/target/release/viu ~/.local/bin/viu
        cd -
    fi

    ##### lua-language-server #####
    if [ -d ~/apps/lua-language-server ]; then
        echo "~/apps/lua-language-server already exists, skipping"
    else
        git clone https://github.com/LuaLS/lua-language-server ~/apps/lua-language-server
        cd ~/apps/lua-language-server
        ./make.sh
    fi

    ##### nvim #####
    if [ -d ~/apps/nvim ]; then
        echo "~/apps/nvim already exists, skipping"
    else
        git clone https://github.com/neovim/neovim ~/apps/nvim
        cd ~/apps/nvim && git checkout nightly && make CMAKE_BUILD_TYPE=Release && cd -
    fi

    if [ -d ~/apps/xkblayout-state ]; then
        echo "~/apps/xkblayout-state already exists, skipping"
    else
        git clone git@github.com:nonpop/xkblayout-state.git ~/apps/xkblayout-state

        cd ~/apps/xkblayout-state/
        make

        rm -f ~/.local/bin/xkblayout-state
        ln -s ~/apps/xkblayout-state/xkblayout-state ~/.local/bin/xkblayout-state
        cd -
    fi

    if ! command -v brave &>/dev/null; then
        echo "brave already exists, skipping"
    else
        sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg

        echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg] https://brave-browser-apt-release.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-release.list

        sudo apt update

        sudo apt install brave-browser
    fi

}

setup_dotfiles() {
    rm -rf ~/.config/i3
    ln -s $(pwd)/dotfiles/i3 ~/.config/i3

    rm -rf ~/.config/autokey
    ln -s $(pwd)/dotfiles/autokey ~/.config/autokey

    rm -rf ~/.config/nvim
    ln -s $(pwd)/dotfiles/nvim ~/.config/nvim
    cd ~/.config/nvim && ./sync_plugins.sh && cd -

    rm -f ~/.bashrc
    ln -s $(pwd)/dotfiles/bashrc ~/.bashrc

    rm -f ~/.bash_profile
    ln -s $(pwd)/dotfiles/bash_profile ~/.bash_profile

    rm -f ~/.tmux.conf
    ln -s $(pwd)/dotfiles/tmux.conf ~/.tmux.conf
    if [ -d ~/apps/nvim ]; then
        echo "~/.tmux/plugins/tpm already exists, skipping"
    else
        git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
    fi

    rm -rf ~/.config/lf
    ln -s $(pwd)/dotfiles/lf ~/.config/lf

    rm -rf ~/.config/qutebrowser
    ln -s $(pwd)/dotfiles/qutebrowser ~/.config/qutebrowser
}

set_default_browser() {
    xdg-settings set default-web-browser firefox-esr.desktop
}

install_apt_programs
install_snap_programs
install_pip_programs
install_npm_programs
install_custom_programs
setup_dotfiles
set_default_browser
