"nnoremap <leader>lc :!pdflatex main.tex<cr>
"nnoremap <leader>lo :!zathura main.pdf &<cr>
let b:ale_linters = ["chktex", "lacheck", "redpen"]
set spell


function LineCommentToggle()
	let colNum = col('.')
	let line = getline('.')
	let start = matchstr(line, '^\s*\zs...')
	normal ^
 
	if start[0] ==? '%'
		normal xx
		let colNum = colNum - 1
	else
		execute "normal i% "
		let colNum = colNum + 2
	endif
	
	call cursor(line('.'), colNum)
endfunction

nnoremap <buffer> <leader>c :call LineCommentToggle()<cr>
noremap \b cw\begin{<C-R>"}<CR>\end{<C-R>"}

set iskeyword-=-
set iskeyword-=:
set iskeyword-=_
