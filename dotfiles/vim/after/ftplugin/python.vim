" setlocal colorcolumn=80

setlocal path=.,**
setlocal wildignore=*.pyc,**/venv/**
setlocal foldmethod=indent

function LineCommentToggle()
	let colNum = col('.')
	let line = getline('.')
	let start = matchstr(line, '^\s*\zs..')
	normal ^
 
	if start[0] ==? '#'
		normal x
		let colNum = colNum - 1
		if start[1] ==? " "
			normal x
			let colNum = colNum - 1
		endif
	else
		execute "normal i# "
		let colNum = colNum + 2
	endif
	
	call cursor(line('.'), colNum)
endfunction



nnoremap <buffer> <leader>c :call LineCommentToggle()<cr>
let b:ale_fixers = ['black']
let b:ale_linters = ["pylint", "cspell"]
