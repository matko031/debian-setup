"""""""""""""" vimtex """""""""""""
let g:vimtex_complete_enabled = 1
let g:vimtex_view_method = 'zathura'
let g:vimtex_quickfix_mode = 0


"""""""""""""" gruvbox """""""""""""
let g:gruvbox_bold = '1'
let g:gruvbox_italic = '1'
let g:gruvbox_transparent_bg = '1'
let g:gruvbox_underline = '1'
let g:gruvbox_underline = '1'
colorscheme gruvbox
set bg=dark

"""""""""""""" vimspector """""""""""""
let g:vimspector_enable_mappings = 'HUMAN'

nnoremap <leader>dd :call vimspector#Launch()<CR>
nnoremap <leader>dx :call vimspector#Reset( { 'interactive': v:true } )<CR>
nmap <leader>de <Plug>VimspectorEval
nmap <leader>dw <Plug>VimspectorWatch
nmap <leader>do <Plug>VimspectorShowOutput
nmap <leader>dn <Plug>VimspectorStepOver
nmap <leader>ds <Plug>VimspectorStepInto
nmap <leader>df <Plug>VimspectorStepOut
nmap <leader>dc <Plug>VimspectorContinue
nmap <leader>dr <Plug>VimspectorRestart
nmap <leader>db <Plug>VimspectorToggleBreakpoint
nmap <Leader>dl <Plug>VimspectorBreakpoints



""""""""""""""" CoC """""""""""""""""""
inoremap <expr> <Tab> coc#pum#visible() ? coc#pum#next(1) : "\<Tab>"
inoremap <expr> <S-Tab> coc#pum#visible() ? coc#pum#prev(1) : "\<S-Tab>"
inoremap <silent><expr> <c-a> coc#refresh()
inoremap <expr> <cr> coc#pum#visible() ? coc#pum#confirm() : "\<CR>"

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)


" Use K to show documentation in preview window.
nnoremap <silent> K :call ShowDocumentation()<CR>

nnoremap <silent> <leader>h :call CocActionAsync('doHover')<cr>

function! ShowDocumentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('doHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction
