" mapping alt+f to turn off search highlight
noremap f :nohlsearch<CR>

" edit vimrc
nnoremap <leader>ve :e $MYVIMRC<cr>
" source vimrc
nnoremap <leader>vs :source $MYVIMRC<cr>


" toggle netrw menu in left split
nnoremap <leader>e :call ToggleLex()<cr>

" show a list of open buffers
nnoremap <leader>u :buffers<cr>:b<space>


" highlight last inserted text
nnoremap gV `[v`]

inoremap jk <ESC>

" Don't overwrite the default register when deleting with x
nnoremap x "_x

" Search in current line only
nnoremap <leader>/ V/\%V
nnoremap <leader>? V?\%V

nnoremap <leader>" :call ToggleSurroundCharacters('"', '"')<CR>
nnoremap <leader>' :call ToggleSurroundCharacters("'", "'")<CR>
vnoremap <leader>" :call SurroundSelection('"', '"')<CR>
vnoremap <leader>' :call SurroundSelection("'", "'")<CR>

nnoremap <leader>{ :call ToggleSurroundCharacters("{", "}")<CR>
nnoremap <leader>} :call ToggleSurroundCharacters('{', '}')<CR>
vnoremap <leader>{ :call SurroundSelection("{", "}")<CR>
vnoremap <leader>} :call SurroundSelection('{', '}')<CR>

nnoremap <leader>[ :call ToggleSurroundCharacters("[", "]")<CR>
nnoremap <leader>] :call ToggleSurroundCharacters('[', ']')<CR>
vnoremap <leader>[ :call SurroundSelection("[", "]")<CR>
vnoremap <leader>] :call SurroundSelection('[', ']')<CR>

nnoremap <leader>< :call ToggleSurroundCharacters('<', '>')<CR>
nnoremap <leader>> :call ToggleSurroundCharacters('<', '>')<CR>
vnoremap <leader>< :call SurroundSelection('<', '>')<CR>
vnoremap <leader>> :call SurroundSelection('<', '>')<CR>

vnoremap i<leader>w :<c-u>call SelectSubWord()<CR>
onoremap i<leader>w :<c-u>call SelectSubWord()<CR>
nnoremap <silent> <leader>w :call MoveToNextSubWord()<CR>
nnoremap <silent> <leader>b :call MoveToPreviousSubWord()<CR>


nnoremap <leader>c :call ConvertToCamelCase()<CR>
nnoremap <leader>s :call ConvertToSnakeCase()<CR>
":call search('\<\|[A-Z]')<CR>
" testSnakeCasedWordOne

" Select text in next ()
onoremap in( :<c-u>normal! f(vi(<cr>
onoremap in) :<c-u>normal! f(vi(<cr>

" Select text in next [] 
onoremap in[ :<c-u>normal! f[vi[<cr>
onoremap in] :<c-u>normal! f[vi[<cr>

" Select text in next {}
onoremap in{ :<c-u>normal! f{vi{<cr>
onoremap in} :<c-u>normal! f{vi{<cr>


" Terminal
nnoremap <leader>t :term<cr>
nnoremap <leader>vt :vert term<cr>
