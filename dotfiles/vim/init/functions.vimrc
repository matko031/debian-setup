let g:LexOpen=0
function ToggleLex()
    " Toggle netrw menu on the left
    
	if g:LexOpen
		Lex
		let g:LexOpen = 0
	else
		if @%==""
			Lex 30
		else
			Lex 30 %:h
		endif
		let g:LexOpen = 1
	endif
endfunction


function! MyFoldText()
    let nblines = v:foldend - v:foldstart + 1
    let w = winwidth(0) - &foldcolumn - (&number ? 8 : 0)
    let line = getline(v:foldstart)
    let comment = substitute(line, '/\*\|\*/\|{{{\d\=', '', 'g')
    let expansionString = repeat(".", w - strwidth(nblines.comment.'"'))
    let txt = '"' . comment . expansionString . nblines
    return txt
endfunction


function! CreateInPreview()
  let l:filename = input("please enter filename: ")
  execute 'silent !touch ' . b:netrw_curdir.'/'.l:filename 
  redraw!
endf

autocmd filetype netrw call Netrw_mappings()
function! Netrw_mappings()
  noremap <buffer>% :call CreateInPreview()<cr>
endfunction

function ToggleSurroundCharacters(symbol_left, symbol_right)
    let start_col = col('.')
    let start_line = line('.')
    call search ('\<', 'bc')

    let char_left = getline('.')[col('.')-2]
    if char_left ==# a:symbol_left
        normal hx
        let start_col -=  1
    else
        execute "normal i" . a:symbol_left
        let start_col += 1
    endif

     normal e
     let char_right = getline('.')[col('.')]
     if char_right ==# a:symbol_right
         normal lx
     else
         execute "normal a" . a:symbol_right
     endif
    
    call cursor([start_line, start_col])
endfunction

function SurroundSelection(symbol_begin, symbol_end)
    let start_col = col("'<")
    let start_line = line("'<")
    let end_col = col("'>")
    let end_line = line("'>")
    call cursor([start_line, start_col])
    execute "normal i" . a:symbol_begin
    
    messages clear
    if start_line == end_line
        call cursor([end_line, end_col+1])
    else
        call cursor([end_line, end_col])
    endif
    execute "normal a" . a:symbol_end
endfunction


let s:delimiters_search_pattern = '\([A-Z]\)\|\(_\)\|\(-\)\|\(:\)\|\(\.\)'
let s:delimiter_chars = ['_', '-', ':', '.']

function SelectSubWord()
    let curr_line = line('.')
    let sub_pattern = search('\(\<\)\|' . s:delimiters_search_pattern, 'bcp')
    if sub_pattern != 2
        normal l
    endif
    let col_left = col('.')
    let sub_pattern = search('\(\>\)\|' . s:delimiters_search_pattern, 'p')
    let col_right = col('.')
    if col_right != col('$')-1
        let col_right = col_right-1
    endif

    call cursor([curr_line, col_left])
    normal v
    call cursor([curr_line, col_right])
endfunction


function MoveToNextSubWord()
     let sub_pattern = search('\(\>\)\|' . s:delimiters_search_pattern, 'p')
     if sub_pattern != 3
         normal l
     endif
endfunction


function MoveToPreviousSubWord()
    let char = getline('.')[col('.')-2]
    if  index(s:delimiter_chars, char) >= 0
        normal h
    endif
    let sub_pattern = search('\(\<\)\|' . s:delimiters_search_pattern, 'pb')
     if sub_pattern != 2 && sub_pattern != 3
        normal l
    endif
endfunction

" snake_cased_word_here_lol_hello
" camelCasedWordHereAgainOnceMore
function ConvertToCamelCase()
    let start_col = col('.')
    let col_offset = 0
    call search('\<', 'bc')
    let sub_pattern = search('\(\>\)\|\(_\)\|\(-\)', 'p')
    while sub_pattern == 3

        if col('.') < start_col-col_offset
            let col_offset += 1
        endif

        normal xh 
        let char = getline(".")[col(".")-1]
        if char !~# "[A-Z]"
            normal v~
        endif
        let sub_pattern = search('\(\>\)\|\(_\)\|\(-\)', 'p')
    endwhile
    call cursor([line('.'), start_col - col_offset])
endfunction 


function ConvertToSnakeCase()
    let start_col = col('.')
    let col_offset = 0
    call search('\<', 'bc')
    normal vu
    let sub_pattern = search('\(\>\)\|\([A-Z]\)', 'p')
    while sub_pattern == 3

        if col('.') <= start_col+col_offset
            let col_offset += 1
        endif

        normal vui_
        let sub_pattern = search('\(\>\)\|\([A-Z]\)', 'p')
    endwhile
    call cursor([line('.'), start_col + col_offset])
endfunction 

