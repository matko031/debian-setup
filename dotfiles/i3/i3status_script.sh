#!/bin/bash

# shell scipt to prepend i3status with more stuff

function get_layout(){
    xkblayout-state print %s
}

function get_variant(){
    xkblayout-state print %v
}

function get_keyboard(){
    layout="$(get_layout)"
    variant="$(get_variant)"
    if [ "$variant" == "" ]; then
        variant="$layout"
    fi
    echo "${layout}-${variant}"
}


#i3status -c ~/.config/i3/i3status.conf | while:
#echo -n "" > ~/.config/i3/line.txt
#echo -n "" > ~/.config/i3/status.txt
first_pass=1
i3status --config ~/.config/i3/i3status.conf | while :
do
        read line
        if [ "$line" != "{\"version\":1}" -a "$line" != "[" ]; then
            LG=$(get_keyboard)
            if [ ${line:0:1} == "[" ]; then
                status="$(echo $line | jq '. += [{name: "layout", full_text: "'${LG}'"}]')"
            else
                status=",$(echo ${line:1} | jq '. += [{name: "layout", full_text: "'${LG}'"}]')"
            fi
        else
            status=${line}
        fi

        echo "$status"
done
