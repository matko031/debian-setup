#!/bin/bash
bat_files="/sys/class/power_supply/BAT0"
while true; do
    bat_status=$(cat ${bat_files}/status)
    capacity=$(cat "${bat_files}/capacity")
    if [[ ${bat_status}=="Discharging" && ${capacity} -le 15 ]]; then
        notify-send \
            --icon=/usr/local/share/icons/battery_low_dark.png \
            "Low battery" \
            "Only ${capacity}% battery remaining"
    fi
    sleep $(( 60 ))
done
