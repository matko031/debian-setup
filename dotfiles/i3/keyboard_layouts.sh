#!/bin/bash


get_index() {
  value="$1"
  shift
  array=("$@")
  for i in "${!array[@]}"; do
    if [[ "${array[$i]}" = "$value" ]]; then
      echo $i
      return
    fi
  done
  echo 0
}


layouts=(us us hr gr de)
variants=("" "dvorak" "" "")

current_layout=$(xkblayout-state print %s)
current_variant=$(xkblayout-state print %v)
current_index=$(get_index $current_layout "${layouts[@]}")

if [ "$current_variant" = dvorak ]; then
  current_index=1
fi

new_index=$(( ($current_index + 1) % ${#layouts[@]}  ))
setxkbmap -layout "${layouts[$new_index]}" -variant "${variants[$new_index]}"


pkill -SIGRTMIN+1 i3blocks
