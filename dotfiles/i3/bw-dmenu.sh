#!/bin/bash

xcowsay hello

bitwarden-dmenu --dmenu-args='-i' --clear-clipboard 30 --session-timeout 7200 --sync-vault-after 3600 --on-error 'cat > /tmp/test'
