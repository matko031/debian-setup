#!/bin/bash
echo "" > brightness_run
if [ $1 -eq 1 ]; then
	step=0.05
elif [ $1 -eq 2 ]; then
	step=-0.05
fi

brightness=$(xrandr --verbose --current | grep eDP-1 -A5 | tail -n1)
brightness_level=${brightness##* }
new_level=$(echo "${brightness_level} + ${step}" | bc)

if  (( $(echo "$new_level > 1.0" | bc) )); then
	new_level=1.0
elif (( $(echo "$new_level < 0.0" | bc) )); then
	new_level=0.0
fi
xrandr --output eDP-1 --brightness ${new_level}
