#/bin/bash

# automagically set up the external monitor when connected

monitors=()
for monitor in $(xrandr --listmonitors | awk ' /^\s+[0-9]:/ {print $4}'); do
	monitors+=($monitor)
	if [ "$monitor" = "DP1" ]; then
		res=$(xrandr | grep -A1 -m2 "^DP1" | tail -1 | awk '{print $1}')
		xrandr --output eDP1 --mode 1920x1080 --output DP1 --mode $res --primary --right-of eDP1
	fi
done



