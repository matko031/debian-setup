#!/bin/bash
brightness=$(xrandr --verbose --current | grep eDP-1 -A5 | tail -n1)
brightness_level=${brightness##* }
brightness_level=$(echo "$brightness_level*100" | bc)
echo ${brightness_level%???}

