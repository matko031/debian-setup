return {
  'ptzz/lf.vim',
  dependencies = {
    'voldikss/vim-floaterm',
    config = function()
      vim.g.floaterm_height = 0.95
      vim.g.floaterm_width = 0.95
    end,
  },
  keys = {
    { '<leader>fe', ':Lf<CR>', mode = 'n', desc = 'Open [f]ile [e]xplorer' },
  },
  init = function()
    vim.g.lf_map_keys = 0
    vim.g.lf_replace_netrw = 1
    vim.g.floaterm_opener = 'edit'
  end,
}
