return {
  'tpope/vim-fugitive',

  keys = {
    { '<leader>G', ':Git<CR>', mode = 'n', desc = 'Open [G]t window' },
  },
}
