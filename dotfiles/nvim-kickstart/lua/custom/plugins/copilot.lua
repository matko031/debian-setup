return {
  'zbirenbaum/copilot.lua',
  cmd = 'Copilot',
  event = 'InsertEnter',
  opts = { suggestion = { auto_refresh = true, auto_trigger = true, keymap = { accept = '<M-CR>' } } },
}
