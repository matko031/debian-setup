return {
  'airblade/vim-rooter',
  init = function()
    vim.g.rooter_patterns = { '.nvim.projectroot', '.nvim-project-matko.lua' }
  end,
}
