return {
  'stevearc/aerial.nvim',
  opts = {
    -- optionally use on_attach to set keymaps when aerial has attached to a buffer
    on_attach = function(bufnr)
      -- Jump forwards/backwards with '{' and '}'
      -- vim.keymap.set('n', '[[', "<cmd>execute \"normal! m'\" | AerialPrev<CR>", {buffer = bufnr})
      -- vim.keymap.set('n', ']]', "<cmd>execute \"normal! m'\" | AerialNext<CR>", {buffer = bufnr})
    end,

    keymaps = {
      ['<CR>'] = function()
        vim.cmd 'execute "normal! m\'"'
        require('aerial.actions').jump.callback()
      end,
    },

    -- Use symbol tree for folding. Set to true or false to enable/disable
    -- Set to "auto" to manage folds if your previous foldmethod was 'manual'
    -- This can be a filetype map (see :help aerial-filetype-map)
    manage_folds = false,

    -- Disable aerial on files with this many lines
    disable_max_lines = 20000,

    -- When you fold code with za, zo, or zc, update the aerial tree as well.
    -- Only works when manage_folds = true
    link_folds_to_tree = false,

    -- Fold code when you open/collapse symbols in the tree.
    -- Only works when manage_folds = true
    link_tree_to_folds = false,

    filter_kind = false,

    backends = { 'lsp', 'treesitter' },
    layout = {
      default_direction = 'right',
      -- These control the width of the aerial window.
      -- They can be integers or a float between 0 and 1 (e.g. 0.4 for 40%)
      -- min_width and max_width can be a list of mixed types.
      -- max_width = {40, 0.2} means "the lesser of 40 columns or 20% of total"
      max_width = { 70, 0.4 },
      width = nil,
      min_width = 0.2,
    },
    --
    -- When jumping to a symbol, highlight the line for this many ms.
    -- Set to false to disable
    highlight_on_jump = 500,
    nav = {
      win_opts = {
        winblend = 0,
      },
      -- Jump to symbol in source window when the cursor moves
      autojump = false,
      -- Show a preview of the code in the right column, when there are no child symbols
      preview = false,
      -- Keymaps in the nav window
      keymaps = {
        ['<CR>'] = 'actions.jump',
        ['<2-LeftMouse>'] = 'actions.jump',
        ['<C-v>'] = 'actions.jump_vsplit',
        ['<C-s>'] = 'actions.jump_split',
        ['h'] = 'actions.left',
        ['l'] = 'actions.right',
        ['<C-c>'] = 'actions.close',
      },
    },
  },
  keys = {
    { '<leader>oo', '<cmd>AerialToggle!<CR>', mode = 'n' },
  },
}
