return {
  'davidosomething/format-ts-errors.nvim',
  opts = {
    add_markdown = true, -- wrap output with markdown ```ts ``` markers
    start_indent_level = 1, -- initial indent
  },
}
