return {}
--[[
return {
  {
    'CopilotC-Nvim/CopilotChat.nvim',
    branch = 'canary',
    dependencies = {
      { 'zbirenbaum/copilot.lua' }, -- or github/copilot.vim
      { 'nvim-lua/plenary.nvim' }, -- for curl, log wrapper
    },
    build = 'make tiktoken', -- Only on MacOS or Linux
    opts = {
      -- debug = true, -- Enable debugging
      -- See Configuration section for rest
    },

    keys = {
      {
        '<leader>ac',
        function()
          require('CopilotChat').toggle()
        end,
        mode = '',
        desc = '[a]i [c]hat',
      },
      {
        '<leader>ad',
        function()
          require('CopilotChat').ask 'Write documentation for current method'
        end,
        mode = '',
        desc = '[a]i [d]ocument',
      },
    },

    -- See Commands section for default commands if you want to lazy load on them
  },
}
]]
--
