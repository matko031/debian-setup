local theme = 'gruvbox'

if theme == 'tokyonight' then
  return {
    'folke/tokyonight.nvim',
    priority = 1000,
    init = function()
      vim.cmd.colorscheme 'tokyonight-night'
      vim.cmd.hi 'Comment gui=none'
    end,
  }
elseif theme == 'gruvbox' then
  return {
    'ellisonleao/gruvbox.nvim',
    priority = 1000,
    init = function()
      require('gruvbox').setup()
      vim.cmd.colorscheme 'gruvbox'
    end,
  }
end
