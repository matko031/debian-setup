return {
  {
    'nvim-telescope/telescope.nvim',
    --event = 'VimEnter',
    cmd = 'Telescope',
    --branch = '0.1.x',
    dependencies = {
      'nvim-lua/plenary.nvim',
      { -- If encountering errors, see telescope-fzf-native README for installation instructions
        'nvim-telescope/telescope-fzf-native.nvim',

        -- `build` is used to run some command when the plugin is installed/updated.
        -- This is only run then, not every time Neovim starts up.
        build = 'make',

        -- `cond` is a condition used to determine whether this plugin should be
        -- installed and loaded.
        cond = function()
          return vim.fn.executable 'make' == 1
        end,
      },
      { 'nvim-telescope/telescope-ui-select.nvim' },
      { 'nvim-telescope/telescope-live-grep-args.nvim' },

      -- Useful for getting pretty icons, but requires a Nerd Font.
      { 'nvim-tree/nvim-web-devicons', enabled = vim.g.have_nerd_font },
    },
    config = function()
      local builtin = require 'telescope.builtin'
      local lga_actions = require 'telescope-live-grep-args.actions'

      require('telescope').setup {
        extensions = {
          ['ui-select'] = {
            require('telescope.themes').get_dropdown(),
          },
          ['fzf'] = {
            fuzzy = true, -- false will only do exact matching
            override_generic_sorter = true, -- override the generic sorter
            override_file_sorter = true, -- override the file sorter
            case_mode = 'smart_case', -- or "ignore_case" or "respect_case"
            -- the default case_mode is "smart_case"
          },
        },

        defaults = {
          path_display = { 'filename_first' },
          dynamic_preview_title = true,
          vimgrep_arguments = {
            'rg',
            '--color=never',
            '--no-heading',
            '--with-filename',
            '--line-number',
            '--column',
            '--smart-case',
            -- '--no-ignore',
            --'--ignore-file',
            --'bundle.js',
            --'-g',
            --'!.git/',
          },
          mappings = {
            i = {
              ['<C-n>'] = require('telescope.actions').cycle_history_next,
              ['<C-p>'] = require('telescope.actions').cycle_history_prev,
              ['<C-d>'] = require('telescope.actions').delete_buffer,
              ['<C-q>'] = require('telescope-live-grep-args.actions').quote_prompt(),
              ['<C-i>'] = lga_actions.quote_prompt { postfix = ' --iglob ' },
              ['<C-g>'] = lga_actions.quote_prompt { postfix = ' -g ' },
            },
            n = {
              ['<leader>qw'] = require('telescope.actions').close,
              ['<C-d>'] = require('telescope.actions').delete_buffer,
              ['<leader>j'] = require('telescope.actions').preview_scrolling_down,
              ['<leader>k'] = require('telescope.actions').preview_scrolling_up,
              ['<leader>h'] = require('telescope.actions').preview_scrolling_left,
              ['<leader>l'] = require('telescope.actions').preview_scrolling_right,
            },
          },
        },
      }

      pcall(require('telescope').load_extension, 'fzf')
      pcall(require('telescope').load_extension, 'ui-select')
    end,

    keys = {
      {
        '<leader>sh',
        function()
          require('telescope.builtin').help_tags()
        end,
        mode = 'n',
        desc = '[s]earch [h]elp',
      },
      {
        '<space>sh',
        function()
          local text = vim.getVisualSelection()
          require('telescope.builtin').help_tags { default_text = text }
        end,
        mode = 'v',
        desc = '[s]earch [h]elp',
      },

      {
        '<leader>sk',
        function()
          require('telescope.builtin').keymaps()
        end,
        mode = 'n',
        desc = '[s]earch [k]eymaps',
      },

      {
        '<leader>sj',
        function()
          require('telescope.builtin').jumplist { show_line = false, trim_text = false, fname_width = 200 }
        end,
        mode = 'n',
        desc = '[s]earch [j]umplist',
      },

      {
        '<leader>sf',
        function()
          require('telescope.builtin').find_files()
        end,
        mode = 'n',
        desc = '[s]earch [f]iles',
      },
      {
        '<space>sf',
        function()
          local text = vim.getVisualSelection()
          --require("telescope.builtin").live_grep({default_text = text, hidden=true, no_ignore=true})
          require('telescope.builtin').find_files { default_text = text }
        end,
        mode = 'v',
        desc = '[s]earch [f]iles',
      },

      {
        '<leader>ss',
        function()
          require('telescope.builtin').builtin()
        end,
        mode = 'n',
        desc = '[s]earch [s]elect Telescope',
      },
      -- {'n', '<leader>sw', require("telescope.builtin").grep_string, { desc = '[s]earch current [w]ord' }},

      {
        '<leader>sg',
        function()
          require('telescope').extensions.live_grep_args.live_grep_args { hidden = true, no_ignore = true }
        end,
        mode = 'n',
        desc = '[s]earch by [g]rep',
      },
      {

        '<space>sg',
        function()
          local text = vim.getVisualSelection()
          --require("telescope.builtin").live_grep({default_text = text, hidden=true, no_ignore=true}
          require('telescope').extensions.live_grep_args.live_grep_args {
            default_text = text,
            hidden = true,
            no_ignore = true,
          }
        end,
        mode = 'v',
        desc = '[s]earch by [g]rep',
      },

      {
        '<leader>sd',
        function()
          require('telescope.builtin').diagnostics()
        end,
        mode = 'n',
        desc = '[s]earch [d]iagnostics',
      },
      {
        '<leader>sr',
        function()
          require('telescope.builtin').resume()
        end,
        mode = 'n',
        desc = '[s]earch [r]esume',
      },
      {
        '<leader>s.',
        function()
          require('telescope.builtin').oldfiles()
        end,
        mode = 'n',
        desc = '[s]earch Recent Files ("." for repeat)',
      },
      {
        '<leader>sb',
        function()
          require('telescope.builtin').buffers { sort_mru = true }
        end,
        mode = 'n',
        desc = '[s]earch open [b]uffers',
      },

      -- Slightly advanced example of overriding default behavior and theme
      {
        '<leader>s/',
        function()
          -- You can pass additional configuration to Telescope to change the theme, layout, etc.
          require('telescope.builtin').current_buffer_fuzzy_find(require('telescope.themes').get_dropdown {
            winblend = 10,
            previewer = false,
          })
        end,
        mode = 'n',
        desc = '[/] Fuzzily search in current buffer',
      },

      -- It's also possible to pass additional configuration options.
      --  See `:help telescope.require("telescope.builtin").live_grep()` for information about particular keys
      {
        '<leader>so',
        function()
          require('telescope.builtin').live_grep {
            grep_open_files = true,
            prompt_title = 'Live Grep in Open Files',
          }
        end,
        mode = 'n',
        desc = '[s]earch  in [o]pen Files',
      },

      -- Shortcut for searching your Neovim configuration files
      {
        '<leader>sn',
        function()
          require('telescope.builtin').find_files { cwd = vim.fn.stdpath 'config' }
        end,
        mode = 'n',
        desc = '[s]earch [n]eovim files',
      },
    },
  },
}
