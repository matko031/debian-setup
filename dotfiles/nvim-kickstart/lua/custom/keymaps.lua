vim.keymap.set('n', '<A-f>', '<cmd>nohlsearch<CR>')

vim.keymap.set('n', 'n', 'nzz')
vim.keymap.set('n', 'N', 'Nzz')

vim.keymap.set('n', 'gV', '`[v`]')

vim.keymap.set('i', 'jk', '<ESC>')

vim.keymap.set('n', 'x', '"_x')

vim.keymap.set('n', '<leader>h', '<C-w>h', { desc = 'Move focus to the left window' })
vim.keymap.set('n', '<leader>l', '<C-w>l', { desc = 'Move focus to the right window' })
vim.keymap.set('n', '<leader>j', '<C-w>j', { desc = 'Move focus to the lower window' })
vim.keymap.set('n', '<leader>k', '<C-w>k', { desc = 'Move focus to the upper window' })

vim.keymap.set('n', ']q', ':cnext<cr>')
vim.keymap.set('n', '[q', ':cprev<cr>')

vim.keymap.set('n', ']b', ':bnext<cr>')
vim.keymap.set('n', '[b', ':bprev<cr>')

vim.keymap.set('n', '<leader>cw', ':close<cr>')
vim.keymap.set('n', '<leader>cb', ':bd<cr>')
vim.keymap.set('n', '<leader>ct', ':tabclose<cr>')
vim.keymap.set('n', '<leader>cq', ':cclose<cr>')
vim.keymap.set('n', '<leader>oq', ':copen<cr>')

--vim.keymap.set('n', '<leader>x', Hex2dec)

vim.keymap.set('n', '<leader>y', '"+y')
vim.keymap.set('n', '<leader>yy', '"+yy')
vim.keymap.set('n', '<leader>p', '"+p')

vim.keymap.set('v', '<leader>y', '"+y')
vim.keymap.set('v', '<leader>p', '"+p')

vim.keymap.set('n', '<leader>"', ":call ToggleSurroundCharacters('\"', '\"')<CR>")

vim.keymap.set('n', "<leader>'", ':call ToggleSurroundCharacters("\'", "\'")<CR>')
vim.keymap.set('v', '<leader>"', ":call SurroundSelection('\"', '\"')<CR>")
vim.keymap.set('v', "<leader>'", ':call SurroundSelection("\'", "\'")<CR>')

vim.keymap.set('n', '<leader>{', ':call ToggleSurroundCharacters("{", "}")<CR>')
vim.keymap.set('n', '<leader>}', ":call ToggleSurroundCharacters('{', '}')<CR>")
vim.keymap.set('v', '<leader>{', ':call SurroundSelection("{", "}")<CR>')
vim.keymap.set('v', '<leader>}', ":call SurroundSelection('{', '}')<CR>")

vim.keymap.set('n', '<leader>[', ':call ToggleSurroundCharacters("[", "]")<CR>')
vim.keymap.set('n', '<leader>]', ":call ToggleSurroundCharacters('[', ']')<CR>")
vim.keymap.set('v', '<leader>[', ':call SurroundSelection("[", "]")<CR>')
vim.keymap.set('v', '<leader>]', ":call SurroundSelection('[', ']')<CR>")

vim.keymap.set('n', '<leader><', ":call ToggleSurroundCharacters('<', '>')<CR>")
vim.keymap.set('n', '<leader>>', ":call ToggleSurroundCharacters('<', '>')<CR>")
vim.keymap.set('v', '<leader><', ":call SurroundSelection('<', '>')<CR>")
vim.keymap.set('v', '<leader>>', ":call SurroundSelection('<', '>')<CR>")

vim.keymap.set('n', '<leader>w', ':call MoveWord("Word", "w")<CR>', { silent = true })
vim.keymap.set('n', 'w', ':call MoveWord("", "w")<CR>', { silent = true })
vim.keymap.set('n', 'W', ':call MoveWord("", "W")<CR>', { silent = true })

vim.keymap.set('n', '<leader>b', ':call MoveWord("Word", "b")<CR>', { silent = true })
vim.keymap.set('n', 'b', ':call MoveWord("", "b")<CR>', { silent = true })
vim.keymap.set('n', 'B', ':call MoveWord("", "B")<CR>', { silent = true })

vim.keymap.set('v', 'iw', ':<C-U>call SelectWord("")<CR>', { silent = true })
vim.keymap.set('v', 'i<leader>w', ':<C-U>call SelectWord("Word")<CR>', { silent = true })

vim.keymap.set('o', 'iw', ':<C-U>call SelectWord("")<CR>')
vim.keymap.set('o', 'i<leader>w', ':<C-U>call SelectWord("Word")<CR>', { silent = true })

-- Diagnostic keymaps
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, { desc = 'Go to previous [D]iagnostic message' })
vim.keymap.set('n', '[e', function()
  vim.diagnostic.goto_prev { severity = 'ERROR' }
end, { desc = 'Go to previous [D]iagnostic message' })
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, { desc = 'Go to next [D]iagnostic message' })
vim.keymap.set('n', ']e', function()
  vim.diagnostic.goto_next { severity = 'ERROR' }
end, { desc = 'Go to previous [D]iagnostic message' })
vim.keymap.set('n', '<leader>d', vim.diagnostic.open_float, { desc = 'Show diagnostic [E]rror messages' })
vim.keymap.set('n', '<leader>q', vim.diagnostic.setqflist, { desc = 'Open diagnostic [Q]uickfix list' })

-- vim.keymap.set('t', '<Esc><Esc>', '<C-\\><C-n>', { desc = 'Exit terminal mode' })

-- Disable arrow keys in normal mode
vim.keymap.set('n', '<left>', '<cmd>echo "Use h to move!!"<CR>')
vim.keymap.set('n', '<right>', '<cmd>echo "Use l to move!!"<CR>')
vim.keymap.set('n', '<up>', '<cmd>echo "Use k to move!!"<CR>')
vim.keymap.set('n', '<down>', '<cmd>echo "Use j to move!!"<CR>')
