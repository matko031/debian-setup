vim.cmd [[
    " WORD: any sequence of non white characters
    " Word: sequence of [A-Z0-9_-]
    " word: sequence of [A-Z0-9]

   function! IsAllCapital(word)
      if toupper(a:word) ==# a:word  
         return 1
      endif
      return 0
   endfunction

   let s:old_iskeyword = &iskeyword
   function! SetKeywords(type)
      let l:delimiters_word_list = ['_', '-'] 
      let s:old_iskeyword = &iskeyword

      if a:type ==# "Word"
         let l:delimiters_list = l:delimiters_word_list
      else
         let l:delimiters_list = []
      endif

      if ! IsAllCapital(expand("<cword>"))
         if &iskeyword !~# 'A-Z'
            " let l:delimiters_list += ['A-Z']
         endif
      endif

      if len(l:delimiters_list) != 0 
         let l:delimiters_string = join(l:delimiters_list, ",")
         execute 'let &iskeyword = "' . &iskeyword . ',' . l:delimiters_string . '"'
      endif
   endfunction

   function! RestoreKeywords()
      if exists("s:old_iskeyword")
         let &iskeyword = s:old_iskeyword 
      endif
   endfunction

   function! MoveWord(...)
      let l:type = get(a:, 1, "")
      let l:direction = get(a:, 2, "w")

      let [_, l:line_start, l:col_start, _] = getpos('.')
      call SetKeywords(l:type)
      execute 'normal! ' . l:direction
      call RestoreKeywords()
      let [_, l:line_end, l:col_end, _] = getpos('.')

      if getline('.')[col('.')-1] !~# '\v[a-zA-Z0-9]' 
         if l:direction ==# "w"
            if getline('.')[col('.')] =~# '\v[a-zA-Z0-9]' 
               normal l
            endif
         elseif l:direction ==# "b"
            if l:col_end == l:col_start-1
               call MoveWord(l:type, l:direction)
            endif
         endif
      endif
   endfunction


   function! SelectWord(...)
      let l:type = get(a:, 1, "")
      call SetKeywords(l:type)
      execute 'normal! viw'
      call RestoreKeywords()
   endfunction

   function! ChangeDirectory(dir)
      if len(b:projectPath) > 0
         if b:projectPath[len(b:projectPath)-1] != "/"
            let b:projectPath .= "/"
         endif
         let l:newdir = b:projectPath . a:dir 
      else
         let l:newdir = a:dir 
      endif

      execute "cd " . l:newdir
   endfunction

   function! SynGroup()
      let l:s = synID(line('.'), col('.'), 1)
      echo synIDattr(l:s, 'name') . ' -> ' . synIDattr(synIDtrans(l:s), 'name')
   endfun

   function! SynStack()
      if !exists("*synstack")
      return
      endif
      echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
   endfunc

 ]]

function vim.getVisualSelection()
  vim.cmd 'noau normal! "vy"'
  local text = vim.fn.getreg 'v'
  vim.fn.setreg('v', {})

  text = string.gsub(text, '\n', '')
  if #text > 0 then
    return text
  else
    return ''
  end
end

function File_exists(name)
  local f = io.open(name, 'r')
  if f ~= nil then
    io.close(f)
    return true
  else
    return false
  end
end

function Comment_line(comment_left, comment_right)
  local comment_left_escaped = escape_string(comment_left)
  local comment_right_escaped = escape_string(comment_right)
  local line = vim.api.nvim_get_current_line()

  local newline, commented
  if comment_right then
    commented = line:match('^%s*' .. comment_left_escaped .. '.*' .. comment_right_escaped .. '%s*$')
  else
    commented = line:match('^%s*' .. comment_left_escaped)
  end

  if not commented then
    local line_len = line:len()
    local pos_text_start = line:match('^%s*'):len() + 1
    local pos_text_end = line_len - line:match('%s*$'):len()
    local pos_text_len = pos_text_end - pos_text_start

    local text_start = line:sub(0, pos_text_start - 1)
    local text_end = line:sub(pos_text_end + 1, line_len)
    local line_text = line:sub(pos_text_start, pos_text_end)
    if comment_right then
      newline = text_start .. comment_left .. ' ' .. line_text .. ' ' .. comment_right .. text_end
    else
      newline = text_start .. comment_left .. line_text .. text_end
    end
  else
    local line_len = line:len()

    local pos_comment_left_start = line:match('^%s*' .. comment_left_escaped:len() + 1)
    local pos_text_start = pos_comment_left_start + comment_left:len()
    if line:sub(pos_comment_left_start, pos_comment_left_start + comment_left:len() + 1):match(comment_left_escaped .. '%s') then
      pos_text_start = pos_text_start + 1
    end

    if comment_right then
      local pos_comment_right_start = line_len - line:match(comment_right_escaped .. '%s*'):len()
      local pos_text_end = pos_comment_right_start
      if line:sub(pos_comment_right_start - 1, pos_comment_right_start - 1 + comment_right:len() + 1):match('%s' .. comment_left_escaped) then
        pos_text_end = pos_text_end - 1
      end
    end

    local pos_text_end = line_len - line:match('%s*$'):len()
    local pos_text_len = pos_text_end - pos_text_start
  end

  vim.api.nvim_set_current_line(newline)
end

function Escape_string(s)
  return (s:gsub('[%-%.%+%[%]%(%)%$%^%%%?%*]', '%%%1'))
end

function Hex2dec()
  local current_word = vim.fn['expand'] '<cword>'
  print(tonumber(current_word, 16))
end

function Indent(n)
  local write = io.write
  for _ = 0, n - 1 do
    write '\t'
  end
end

function PrintTable(table, n)
  local write = io.write
  n = n or 0

  if n == 0 then
    write '{\n'
    n = n + 1
  end

  for k, v in pairs(table) do
    Indent(n)
    write(k .. ': ')
    if type(v) == 'table' then
      write '{\n'
      PrintTable(v, n + 1)
    else
      write(v .. '\n')
    end
  end
  n = n - 1
  Indent(n)
  write '}\n'
end
