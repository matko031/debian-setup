-- Set <space> as the leader key
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

vim.g.have_nerd_font = false -- Used by lazy ui

vim.opt.shiftwidth = 4 -- Number of spaces inserted when using >>, <<, and auto indent
vim.opt.expandtab = true -- Insert spaces instead of <Tab> when tab key is pressed
vim.opt.tabstop = 4 -- 1 <Tab> is rendered as <tabstop> spaces
vim.opt.softtabstop = 4 -- When <Tab> is pressed, insert a combination of \t and spaces to move `softtabstop` columns to the right
vim.opt.smarttab = true -- only really useful if softtabstop is different from shiftwidth
vim.opt.autoindent = true -- copy current indent to newline
vim.opt.smartindent = true -- automatically increase indent for example on newline after '{'
vim.opt.hidden = true

vim.opt.listchars = { -- see hidden chars
  tab = '<->',
  eol = '¬',
  trail = '·',
  nbsp = '␣',
}
vim.opt.list = true

vim.opt.number = true
vim.opt.relativenumber = true

vim.opt.swapfile = false
vim.opt.autowriteall = true -- automatically write modified file

vim.opt.mouse = 'a' -- enable mouse in all modes

vim.opt.showmode = false -- Don't show the mode

--vim.opt.clipboard = 'unnamedplus' -- share clipboard between vim and OS

vim.opt.breakindent = true -- keep indent on wrapped/long lines

vim.opt.undofile = true -- Save undo history

vim.opt.ignorecase = true -- \c in pattern -> ignorecase=true, \C in pattern -> ignorecase=false
vim.opt.smartcase = true -- override ignorecase if pattern contains upper case letters

vim.opt.signcolumn = 'yes' -- always show sign column

vim.opt.updatetime = 1000 -- Decrease swapfile update time and cursorhold time

vim.opt.timeoutlen = 100 -- Decrease mapped sequence wait time

-- Configure how new splits should be opened
vim.opt.splitright = true
vim.opt.splitbelow = true

vim.opt.wrapscan = false -- don't wrap around end of file when searching

vim.opt.inccommand = 'split' -- show off screen substitutions in a split

vim.opt.cursorline = true -- Show which line your cursor is on

vim.opt.scrolloff = 5 -- Minimal number of screen lines to keep above and below the cursor.

vim.opt.hlsearch = true -- Set highlight on search

-- folding settings
vim.opt.foldmethod = 'expr'
vim.opt.foldexpr = 'v:lua.vim.treesitter.foldexpr()'
--vim.opt.foldtext = 'v:lua.vim.treesitter.foldtext()'
vim.opt.foldcolumn = 'auto:3'

vim.o.shell = 'bash' -- shell to use for ! commands

vim.opt.iskeyword:remove { '_' }

--vim.o.statusline = '%F' .. '%=' .. "%{fnamemodify(getcwd(),':~')} %l/%L"

vim.diagnostic.config {
  float = {
    border = 'rounded', -- Rounded borders for better aesthetics
    -- source = true, -- Show the source of the diagnostic
    --[[
    format = function(diagnostic)
      -- Use markdown-compatible formatting
      return string.format('**%s**\n\n%s', diagnostic.source, diagnostic.message)
    end,
        --]]
  },
}
